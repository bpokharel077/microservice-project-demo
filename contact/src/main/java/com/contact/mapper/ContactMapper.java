package com.contact.mapper;

import com.contact.dto.ContactDto;
import com.contact.entities.Contact;
import org.springframework.stereotype.Component;

@Component
public class ContactMapper {
    public Contact contactDtoToContact(ContactDto contactDto){
        Contact contact= new Contact();
        contact.setId(contactDto.getId());
        contact.setEmail(contactDto.getEmail());
        contact.setLocation(contactDto.getLocation());
        contact.setPhoneNumber(contactDto.getPhoneNumber());
        contact.setUserId(contactDto.getUserId());
        return contact;
    }

    public ContactDto contactTOContactDto(Contact contact){
        return ContactDto.builder()
                .id(contact.getId())
                .email(contact.getEmail())
                .location(contact.getLocation())
                .phoneNumber(contact.getPhoneNumber())
                .userId(contact.getUserId())
                .build();
    }
}
