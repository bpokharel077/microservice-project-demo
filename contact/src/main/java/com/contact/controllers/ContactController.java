package com.contact.controllers;

import com.contact.abstracts.BaseClass;
import com.contact.dto.ContactDto;
import com.contact.dto.GlobalApiResponse;
import com.contact.service.ContactService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContactController extends BaseClass {
    ContactService contactService;

    ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping()
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity(successResponse("Data fetch successful.", contactService.getAllContacts()), HttpStatus.OK);
    }

      /* @GetMapping("/contact-user/{user-id}")
       public GlobalApiResponse getContactByUserId(@PathVariable("user-id") int userId) {
           if (contactService.getContactByUserId(userId) != null)
               return successResponse("Data fetched successfully.", contactService.getContactByUserId(userId));
           else
               return errorResponse("User doesn't have contact registered.", userId);
       }*/
    @GetMapping("/contact-user/{user-id}")
    public ContactDto getContactByUserId(@PathVariable("user-id") int userId) {
        return contactService.getContactByUserId(userId);
    }

    @PostMapping()
    public ResponseEntity<GlobalApiResponse> save(@RequestBody ContactDto contactDto) {
        return new ResponseEntity(successResponse("Data saved successfully.", contactService.save(contactDto)), HttpStatus.OK);
    }
    @GetMapping("/add-contact/{user-id}")
    public ContactDto addContactWithUserId(@PathVariable("user-id") int userId) {
        return contactService.save(ContactDto.builder()
                .userId(userId)
                .phoneNumber("0987654321")
                .location("KTM")
                .email("test@gmail.com")
                .build());
    }
}
