package com.contact.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto {
    private int id;

    private String location;

    private String phoneNumber;

    private String email;

    private int userId;
}
