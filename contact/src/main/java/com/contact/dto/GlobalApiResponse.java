package com.contact.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GlobalApiResponse {
    private boolean status;
    private String message;
    private Object data;
}
