package com.contact.service;

import com.contact.dto.ContactDto;
import com.contact.entities.Contact;

import java.util.List;

public interface ContactService {
    List<ContactDto> getAllContacts();
    ContactDto getContactByUserId(int userId);
    ContactDto save(ContactDto contactDto);
}
