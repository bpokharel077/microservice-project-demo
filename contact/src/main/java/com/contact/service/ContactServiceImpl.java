package com.contact.service;

import com.contact.dto.ContactDto;
import com.contact.entities.Contact;
import com.contact.mapper.ContactMapper;
import com.contact.repository.ContactRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {
    private ContactRepository contactRepository;
    private ContactMapper contactMapper;

    ContactServiceImpl(ContactRepository contactRepository, ContactMapper contactMapper) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
    }

    @Override
    public List<ContactDto> getAllContacts() {
        List<Contact> contactList = contactRepository.findAll();
        List<ContactDto> contactDtoList = new ArrayList<>();
        for (Contact contact : contactList) {
            contactDtoList.add(contactMapper.contactTOContactDto(contact));
        }
        return contactDtoList;
    }

    @Override
    public ContactDto getContactByUserId(int userId) {
        return contactMapper.contactTOContactDto(contactRepository.getContactByUserId(userId));
    }

    @Override
    public ContactDto save(ContactDto contactDto) {
        return contactMapper.contactTOContactDto(contactRepository.save(contactMapper.contactDtoToContact(contactDto)));
    }
}
