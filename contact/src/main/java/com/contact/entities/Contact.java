package com.contact.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Table(name = "contact_tbl")
@Getter
@Setter
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contactSeqGen")
    @SequenceGenerator(name = "contactSeqGen", sequenceName = "contactSeq", allocationSize = 1)
    private int id;
    @Column(name = "location")
    private String location;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    @Email(message = "Input should be in email format")
    private String email;

    @Column(name = "user_id")
    private int userId;
}
