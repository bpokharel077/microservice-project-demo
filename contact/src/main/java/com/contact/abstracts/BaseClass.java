package com.contact.abstracts;

import com.contact.dto.GlobalApiResponse;

public class BaseClass {
    protected GlobalApiResponse successResponse(String message, Object data){
        return GlobalApiResponse.builder()
                .status(true)
                .message(message)
                .data(data)
                .build();
    }
    protected GlobalApiResponse errorResponse(String message, Object data){
        return GlobalApiResponse.builder()
                .status(false)
                .message(message)
                .data(data)
                .build();
    }
}
