package com.microservices.user.services;

import com.microservices.user.dtos.UserDto;
import com.microservices.user.entities.User;
import com.microservices.user.repo.UserRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User saveUser(UserDto userDto) {
        User user=userMapping(userDto);
        return userRepo.save(user);
    }
    private User userMapping(UserDto userDto)
    {
       return User.builder()
               .id(userDto.getId())
               .userName(userDto.getUserName())
               .age(userDto.getAge()).build();
    }

    @Override
    public List<UserDto> getAllUser() {
        List<User> userList= userRepo.findAll();
        List<UserDto> userDtoList= new ArrayList<>() ;
        for(User user:userList)
        {
           UserDto userDto=  UserToDtoMapping(user);
           userDtoList.add(userDto);
        }
        return userDtoList;
    }
    private UserDto UserToDtoMapping(User user)
    {
        return UserDto.builder()
                .id(user.getId())
                .userName(user.getUserName())
                .age(user.getAge())
                .build();
    }
}
