package com.microservices.user.services;

import com.microservices.user.dtos.UserDto;
import com.microservices.user.entities.User;

import java.util.List;

public interface UserService {
    User saveUser(UserDto userDto);
    List<UserDto> getAllUser();
}
