package com.microservices.user.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactDto {
    private int id;

    private String location;

    private String phoneNumber;

    private String email;

    private int userId;
}
