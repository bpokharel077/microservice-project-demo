package com.microservices.user.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserDto {
    private Integer id;

    private String userName;

    private Integer age;

    private ContactDto contact;
}
