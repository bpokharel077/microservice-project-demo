package com.microservices.user.config;

import com.microservices.user.dtos.ContactDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "contact-service")
public interface ContactFeignClient {
    @GetMapping("/contact-user/{user-id}")
     ContactDto getContact(@PathVariable("user-id") int userId);
}
