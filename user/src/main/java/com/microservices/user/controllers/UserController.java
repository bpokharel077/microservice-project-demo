package com.microservices.user.controllers;

import com.microservices.user.abstracts.BaseClass;
import com.microservices.user.config.ContactFeignClient;
import com.microservices.user.dtos.ContactDto;
import com.microservices.user.dtos.GlobalApiResponse;
import com.microservices.user.dtos.UserDto;
import com.microservices.user.entities.User;
import com.microservices.user.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@RestController
@RequestMapping("user")

public class UserController extends BaseClass {
    private final UserService userService;
    private final RestTemplate restTemplate;
    private final WebClient.Builder webClientBuilder;
    private final ContactFeignClient contactFeignClient;

    public UserController(UserService userService, RestTemplate restTemplate, WebClient.Builder webClientBuilder, ContactFeignClient contactFeignClient) {
        this.userService = userService;
        this.restTemplate = restTemplate;
        this.webClientBuilder = webClientBuilder;
        this.contactFeignClient = contactFeignClient;
    }

    //using Feign Client
    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAllUser() {
        List<UserDto> userDtoList = userService.getAllUser();
        ContactDto contactDto;
        for (UserDto userDto : userDtoList) {
            contactDto = contactFeignClient.getContact(userDto.getId());
            userDto.setContact(contactDto);
        }
        return new ResponseEntity<>(successResponse("User info fetched successfully", userDtoList), HttpStatus.OK);
    }


//using RestTemplet
/*    @GetMapping
    //@HystrixCommand(fallbackMethod="")
    public ResponseEntity<GlobalApiResponse> getAllUser() {

        List<UserDto> userDtoList = userService.getAllUser();
        ContactDto contactDto;
        for (UserDto userDto : userDtoList) {
            contactDto = restTemplate.getForObject("http://contact-service/contact-user/" + userDto.getId(), ContactDto.class);
            userDto.setContact(contactDto);
        }
        //contactDto=restTemplate.getForObject("http://localhost:8080/contact-user/4",ContactDto.class);
        return new ResponseEntity<>(successResponse("User info fetched successfully", userDtoList), HttpStatus.OK);
    }*/
    /*@GetMapping
    public ResponseEntity<GlobalApiResponse> getAllUser() {

        List<UserDto> userDtoList = userService.getAllUser();
        ContactDto contactDto =new ContactDto();
        GlobalApiResponse globalApiResponse = new GlobalApiResponse();
        for (UserDto userDto : userDtoList) {
            globalApiResponse = restTemplate.getForObject("http://localhost:8080/contact-user/" + userDto.getId(), GlobalApiResponse.class);
            //BeanUtils.copyProperties(globalApiResponse.getData(),contactDto);
            //ObjectMapper.convertValue();
            userDto.setContact(contactDto);
            //userDtoList.add(userDto);
        }
        //contactDto=restTemplate.getForObject("http://localhost:8080/contact-user/4",ContactDto.class);
        return new ResponseEntity<>(successResponse("User info fetched successfully", userDtoList), HttpStatus.OK);
    }*/

    @PostMapping
    public ResponseEntity<GlobalApiResponse> create(@RequestBody UserDto userDto) {
        User user = userService.saveUser(userDto);
        ContactDto contactDto = new ContactDto();
        contactDto = restTemplate.getForObject("http://contact-service/add-contact/" + user.getId(), ContactDto.class);
        return new ResponseEntity<>(successResponse("User saved successfully", user), HttpStatus.OK);
    }
}
